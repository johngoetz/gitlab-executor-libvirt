import argparse
import logging
import multiprocessing
import pathlib
import pprint
import shutil
import socket
import stat
import subprocess

import gitlab
import tomlkit


log = logging.getLogger(__name__)


def register_libvirt_executor(install_prefix, gitlab_url, auth_token,
                              registration_token, config_file, mode, paused,
                              dry_run):
    bindir = (install_prefix/'bin').expanduser()
    hostname = socket.gethostname().split('.')[0]
    homedir = pathlib.Path('~').expanduser()
    if config_file.is_file():
        with open(config_file) as f:
            cfg = tomlkit.parse(f.read())
            if cfg['concurrent'] == 1:
                resources = 'reserved'
            else:
                resources = 'shared'
    else:
        resources = 'reserved'

    runner_name = f'{hostname}-libvirt'
    limit = max(1, int(multiprocessing.cpu_count() // 4) - 1)
    tags = ['libvirt', 'virtual', mode, hostname, resources]
    params = {
        'config-exec': bindir/'ci-libvirt-config.sh',
        'config-exec-timeout': 3 * 60,
        'prepare-exec': bindir/'ci-libvirt-prepare.sh',
        'prepare-exec-timeout': 30 * 60,
        'run-exec': bindir/'ci-libvirt-run.sh',
        'cleanup-exec': bindir/'ci-libvirt-cleanup.sh',
        'cleanup-exec-timeout': 5 * 60,
        'graceful-kill-timeout': 1 * 60,
        'force-kill-timeout': 3 * 60,
    }

    gl = gitlab.Gitlab(gitlab_url, auth_token)

    runners = gl.runners.all(type='instance_type', all=True)
    runners = list(filter(lambda r: r.description == runner_name, runners))
    if len(runners) > 1:
        raise SystemError(f'multiple runners found with name {runner_name}')
    elif len(runners) == 1:
        runner = gl.runners.get(runners[0].id)
    else:
        runner = None

    if runner is None:
        log.info(f'registering new runner {runner_name}')
        cmd = ['gitlab-runner', 'register', '--non-interactive',
               '--config', config_file, '--url', gitlab_url,
               '--registration-token', registration_token,
               '--executor', 'custom', '--name', runner_name,
               '--tag-list', ','.join(tags), '--limit', str(limit),
               '--builds-dir', '/ci/builds', '--cache-dir', '/ci/cache']
        if paused:
            cmd += ['--paused']
        for k, v in params.items():
            cmd.extend((f'--custom-{k}', str(v)))

        if dry_run:
            print(' '.join(map(str, cmd)))
        else:
            log.debug(f'running command: {" ".join(map(str, cmd))}')
            subprocess.run(cmd, check=True)
    else:
        with open(config_file, 'r+') as f:
            cfg = tomlkit.parse(f.read())
            fltr = lambda r: r['name'] == runner_name
            runnercfg = list(filter(fltr, cfg['runners']))[0]
            runnercfg['limit'] = limit
            for k, v in params.items():
                if not isinstance(v, (str, int)):
                    v = str(v)
                runnercfg['custom'][k.replace('-', '_')] = v
            if dry_run:
                print(f'New config for {runner_name} in {config_file}:')
                pprint.pprint(runnercfg)
            else:
                log.debug(f'writing out new config to {config_file}')
                f.seek(0)
                f.write(tomlkit.dumps(cfg, sort_keys=True))
                f.truncate()

        runner.active = not paused
        runner.tag_list = tags
        if dry_run:
            print('setting runner {runner_name} parameters:')
            print(f'active: {runner.active}')
            print(f'tag list: {runner.tag_list}')
        else:
            runner.save()


def install_libvirt_executor(install_prefix, dry_run):
    here = pathlib.Path(__file__).parent
    bindir = install_prefix/'bin'
    if dry_run:
        print(f'mkdir -p {bindir}')
    else:
        bindir.mkdir(parents=True, exist_ok=True)
    for script in (here/'scripts').glob('*.sh'):
        if dry_run:
            print(f'cp {script} {bindir}/')
        else:
            shutil.copy(script, bindir)


def secret(item):
    item = pathlib.Path(item).expanduser()
    if not item.is_file():
        return str(item).strip()
    else:
        mode = item.stat().st_mode
        if mode & (stat.S_IRGRP | stat.S_IROTH):
            msg = f'{item} must be exclusively readable by the owner.'
            raise OSError(msg)
        return item.read_text().strip()


def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(
        '--install-prefix', default='/usr/local',
        type=lambda p: pathlib.Path(p).expanduser(),
        help='''Installation destination. Scripts will be copies into the bin
                subdirectory here.''')
    parser.add_argument(
        '--gitlab-url', default='https://gitlab.com',
        help='''GitLab server URL.''')
    parser.add_argument(
        '--registration-token', default='~/.gitlab-registration-token',
        type=secret,
        help='''Runner registration token or file containing it. The file must
                be exclusively readable by the owner.''')
    parser.add_argument(
        '--auth-token', default='~/.gitlab-auth-token', type=secret,
        help='''GitLab authentication token or file containing it. The file
                must be exclusively readable by the owner.''')
    parser.add_argument(
        '--config-file', default='~/gitlab-runner-config.toml',
        type=lambda p: pathlib.Path(p).expanduser(),
        help='''Runner configuration file to use.''')
    parser.add_argument(
        '--mode', choices=('production', 'staging'), default='production',
        help='''Production level to set the executor.''')
    parser.add_argument(
        '--paused', action='store_true',
        help='''Put the runner into a paused state.''')
    parser.add_argument(
        '--dry-run', action='store_true',
        help='''Only print commands to run, do not execute them.''')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    install_libvirt_executor(args.install_prefix, args.dry_run)
    register_libvirt_executor(
        args.install_prefix, args.gitlab_url, args.auth_token,
        args.registration_token, args.config_file, args.mode,
        args.paused, args.dry_run)
