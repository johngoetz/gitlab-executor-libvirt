# Libvirt (QEMU) GitLab Custom Executor

A GitLab custom executor using QEMU virtual machines orchestrated with
libvirt.

This will work with Linux and Windows VMs running on a Linux host using QEMU
and qcow2 images. A job typically specifies an `image` which identifies the
libvirt "domain" to clone from. A linked (copy-on-write) clone of the virtual
machine is created for each job. The VMs must be defined in `qemu:///session`
and all communication is done through SSH.

## Installation and Requirements

The individual scripts, found under the `scripts` directory, may be copied into
a location such as `/usr/local/bin` and pointed to during registration of the
runner with the `gitlab-runner register` command. The associated software
requirements are:

    * Bash
    * QEMU
    * libvirt
    * gitlab-runner

To assist in installation and runner configuration, the `install-executor.py`
script can be used which requires Python 3.6+. See the output of `python
install-executor.py --help` for more information.

The installation and runner registration commands typically look like this:

```
cp scripts/ci-libvirt-*.sh /usr/local/bin/
gitlab-runner register --non-interactive                         \
    --config ~/gitlab-runner-config.toml                         \
    --url https://gitlab.com                                     \
    --registration-token [REGISTRATION_TOKEN]                    \
    --executor custom                                            \
    --name `hostname -s`-libvirt                                 \
    --tag-list libvirt,virtual,production,`hostname -s`,reserved \
    --limit 1                                                    \
    --builds-dir /ci/builds                                      \
    --cache-dir /ci/cache                                        \
    --custom-config-exec /usr/local/bin/ci-libvirt-config.sh     \
    --custom-config-exec-timeout 180                             \
    --custom-prepare-exec /usr/local/bin/ci-libvirt-prepare.sh   \
    --custom-prepare-exec-timeout 1800                           \
    --custom-run-exec /usr/local/bin/ci-libvirt-run.sh           \
    --custom-cleanup-exec /usr/local/bin/ci-libvirt-cleanup.sh   \
    --custom-cleanup-exec-timeout 300                            \
    --custom-graceful-kill-timeout 60                            \
    --custom-force-kill-timeout 180
```

The image specified must be a registered libvirt domain in the QEMU
user-session `qemu:///session`. This

## Usage

For a job to use this runner, the tags must match of couse and should supply
the libvirt domain to use via the `image` keyword in the pipeline
configuration:

```
release-build:
  stage: build
  tags: [libvirt, production]
  image: windows-10-build
  artifacts:
    paths:
      - build/**/*
  script:
    - cmake.exe -B build -S src
    - cmake.exe --build build --config Release
```

The `windows-10-build` libvirt domain (VM) will be cloned. All disk devices
will be copy-on-write-linked to the devices on the original domain. This means
the original VM must be kept read-only while in use.

## Configuration

These are the environment variables that can be set in the CI pipeline
configuration that control how the job is executed. These are defined in
the `ci-libvirt-config.sh` script.

"CI_JOB_IMAGE": "$CI_JOB_IMAGE",
"CI_JOB_CMD": "${CUSTOM_ENV_CI_JOB_CMD:-bash -l}",

"CI_IMAGE_OS": "$CI_IMAGE_OS",
"CI_IMAGE_NAME": "$CI_IMAGE_NAME",

"CI_SSH_USER": "${CUSTOM_ENV_CI_SSH_USER:-runner}",
"CI_SSH_PASSWORD": "${CUSTOM_ENV_CI_SSH_PASSWORD:-vagrant}",
"CI_NET_DEVICE": "virbr0",

"CI_BUILDS_DIR": "$CI_BUILDS_DIR",
"CI_CACHE_DIR": "$CI_CACHE_DIR",

"CI_BUILD_FAILURE_EXIT_CODES": "${CUSTOM_ENV_CI_BUILD_FAILURE_EXIT_CODES:-1}",
"CI_SYSTEM_FAILURE_EXIT_CODES": "${CUSTOM_ENV_CI_BUILD_FAILURE_EXIT_CODES:-255}",
"CI_DEFAULT_FAILURE_EXIT_CODE": "${CUSTOM_ENV_CI_DEFAULT_FAILURE_EXIT_CODE:-$SYSTEM_FAILURE_EXIT_CODE}",

"CI_TIMEOUT": "$(( 8*60*60 ))",
"CI_LOG_LEVEL": "$CI_LOG_LEVEL"
