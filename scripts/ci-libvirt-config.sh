#!/bin/bash

set -eo pipefail
trap 'exit $SYSTEM_FAILURE_EXIT_CODE' ERR

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$HERE/ci-libvirt-lib.sh"

CI_LOG_LEVEL="${CUSTOM_ENV_CI_LOG_LEVEL:-info}"
CI_JOB_IMAGE="${CUSTOM_ENV_CI_JOB_IMAGE:-alpine}"

# TODO: Fetch or ensure that CI_JOB_IMAGE is the latest
#...

IMAGE_INFO=$(virt-inspector -d "$CI_JOB_IMAGE")
CI_IMAGE_OS=$(virt-inspector --xpath 'string(/operatingsystems/operatingsystem/name)' <<< "$IMAGE_INFO")

if [[ "$CI_IMAGE_OS" == "windows" ]]; then
    CI_BUILDS_DIR="${CUSTOM_ENV_CI_BUILDS_DIR:-/mnt/c/ci/builds}"
    CI_CACHE_DIR="${CUSTOM_ENV_CI_CACHE_DIR:-/mnt/c/ci/cache}"
else
    CI_BUILDS_DIR="${CUSTOM_ENV_CI_BUILDS_DIR:-/ci/builds}"
    CI_CACHE_DIR="${CUSTOM_ENV_CI_CACHE_DIR:-/ci/cache}"
fi

# Replace spaces and punctuation in the stage and job names with underscores.
#STAGE_NAME=$(echo "$CUSTOM_ENV_CI_JOB_STAGE" | tr '[:blank:][:punct:]' '_')
#JOB_NAME=$(echo "$CUSTOM_ENV_CI_JOB_NAME" | tr '[:blank:][:punct:]' '_')

# The VM image name for this job.
NAME="$CI_JOB_IMAGE-$CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN"
NAME="$NAME-$CUSTOM_ENV_CI_CONCURRENT_ID"
#NAME="$NAME-project-$CUSTOM_ENV_CI_PROJECT_ID"
#NAME="$NAME-stage-$STAGE_NAME"
#NAME="$NAME-job-$JOB_NAME"
CI_IMAGE_NAME="$NAME"

cat << EOS
{
  "driver": {
    "name": "libvirt",
    "version": "v0.6.0"
  },

  "builds_dir": "$CI_BUILDS_DIR",
  "cache_dir": "$CI_CACHE_DIR",
  "builds_dir_is_shared": false,

  "job_env": {
    "CI_JOB_IMAGE": "$CI_JOB_IMAGE",
    "CI_JOB_CMD": "${CUSTOM_ENV_CI_JOB_CMD:-bash -l}",

    "CI_IMAGE_OS": "$CI_IMAGE_OS",
    "CI_IMAGE_NAME": "$CI_IMAGE_NAME",

    "CI_SSH_USER": "${CUSTOM_ENV_CI_SSH_USER:-runner}",
    "CI_SSH_PASSWORD": "${CUSTOM_ENV_CI_SSH_PASSWORD:-vagrant}",
    "CI_NET_DEVICE": "virbr0",

    "CI_BUILDS_DIR": "$CI_BUILDS_DIR",
    "CI_CACHE_DIR": "$CI_CACHE_DIR",

    "CI_BUILD_FAILURE_EXIT_CODES": "${CUSTOM_ENV_CI_BUILD_FAILURE_EXIT_CODES:-1}",
    "CI_SYSTEM_FAILURE_EXIT_CODES": "${CUSTOM_ENV_CI_BUILD_FAILURE_EXIT_CODES:-255}",
    "CI_DEFAULT_FAILURE_EXIT_CODE": "${CUSTOM_ENV_CI_DEFAULT_FAILURE_EXIT_CODE:-$SYSTEM_FAILURE_EXIT_CODE}",

    "CI_TIMEOUT": "$(( 8*60*60 ))",
    "CI_LOG_LEVEL": "$CI_LOG_LEVEL"
  }
}
EOS
