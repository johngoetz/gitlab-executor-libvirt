#!/bin/bash

SCRIPT="$1"
STEP="$2"

set +eo pipefail

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$HERE/ci-libvirt-lib.sh"

#
# Order of steps intiated by the GitLab-CI system
#
# GITLAB SCRIPTS
#    prepare_script
#    get_sources (git)
#    restore_cache (gitlab-runner)
#    download_artifacts (gitlab-runner)
# USER SCRIPTS
#    step_*
#    build_script
#    step_*
#    after_script
# GITLAB SCRIPTS
#    archive_cache OR archive_cache_on_failure (gitlab-runner)
#    upload_artifacts_on_success OR upload_artifacts_on_failure (gitlab-runner)
#    cleanup_file_variables
#
# Errors will always return $SYSTEM_FAILURE_EXIT_CODE except for
# non-zero exits from the "step_script" and "build_script" steps.
# This these cases, the return code will be:
#   1. $BUILD_FAILURE_EXIT_CODE if the script return code is in the list
#      defined in $CI_BUILD_FAILURE_EXIT_CODES
#   2. $SYSTEM_FAILURE_EXIT_CODE if the script return code is in the list
#      defined in $CI_SYSTEM_FAILURE_EXIT_CODES
#   3. $CI_DEFAULT_FAILURE_EXIT_CODE for all other return codes as defined
#      in the ci-libvirt-config.sh script
#
log debug "step: $STEP"

IPADDR=$(vm-ip-addr "$CI_NET_DEVICE" "$CI_IMAGE_NAME")
alias ssh-exec="ssh ${SSH_PRIVATE_KEY_OPTS[*]} $CI_SSH_USER@$IPADDR"

ssh-exec "${CI_JOB_CMD[@]}" < "$SCRIPT"
RETURN_CODE=$?

if [[ $RETURN_CODE -ne 0 ]]; then
    if [[ "$STEP" == "step_script" || "$STEP" == "build_script" ]]; then
        if grep -qw $RETURN_CODE <<< "$CI_BUILD_FAILURE_EXIT_CODES"; then
            log error "build failure with return code: $RETURN_CODE"
            exit $BUILD_FAILURE_EXIT_CODE
        elif grep -qw $RETURN_CODE <<< "$CI_SYSTEM_FAILURE_EXIT_CODES"; then
            log error "system failure with return code: $RETURN_CODE"
            exit $SYSTEM_FAILURE_EXIT_CODE
        else
            log error "failure with unknown return code: $RETURN_CODE"
            log error "exiting with default code: $CI_DEFAULT_FAILURE_EXIT_CODE"
            exit $CI_DEFAULT_FAILURE_EXIT_CODE
        fi
    else
        log error "system failure with return code: $RETURN_CODE"
        exit $SYSTEM_FAILURE_EXIT_CODE
    fi
fi
